# Contribuição

Caso esteja buscando um tema para contribuir, confira informações de backlog em [HELPWANTED.md](HELPWANTED.md)

## Contribuindo com este Produto

Passo-a-passo para contribuir com este código:

1. Criar uma nova feature branch.
2. Realizar as alterações desejadas.
3. Entrar em contato com os Maintainers através de uma [Issue no GitLab](https://gitcorp.prod.cloud.ihf/groups/SW6/modulos/-/issues).*
4. Atualizar a documentação do produto caso necessário.
5. Realizar Merge Request para a branch **develop** informando o Issue correspondente.
6. Entrar em contato com o Maintainer para realização do code review, revisão da documentação e aprovação do Merge Request.

- *Caso não haja Issue aberta, efetuar criação de uma nova tarefa para cobrir sua necessidade.

Com essa alteração aprovada, será gerada uma **nova tag** para disponibilização da solução ou feature desenvolvida.

Confira o git-flow detalhado neste diagrama:
![git flow innersource](git-flow-innersource.png)

## Nossos Produtos
- [Authorizer](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-authorizer)
- [Certmanager](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-certmanager)
- [ChaosMesh](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-chaosmesh)
- [Istio](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-istio)
- [JaegerCollector](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-collector)
- [Kiali](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-kiali)
- [Mesh](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-mesh)
- [NLB](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-nlb)
- [Prometheus](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-prometheus)
- [argocd](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-argocd)
- [Vault](https://gitcorp.prod.cloud.ihf/SW6/modulos/servicemesh-vault)

## Sobre InnerSource

- [InnerSource de IaC da Comunidade Cloud](https://confluencecorp.ctsp.prod.cloud.ihf/display/JCP/InnerSource+-+Infra+as+Code)
- [Portal de InnerSource do Itaú](https://iconectados.sharepoint.com/sites/InnerSourceItaUnibanco?OR=Teams-HL&CT=1630690037994)

## Contatos

Squad Poseidon:
  - [CID_Service_Mesh](CID_Service_Mesh@correio.itau.com.br)
  - [Mantenedores](https://confluencecorp.ctsp.prod.cloud.ihf/pages/viewpage.action?pageId=461962771)
  - [Nossa Jornada](https://confluencecorp.ctsp.prod.cloud.ihf/pages/viewpage.action?pageId=526056542)